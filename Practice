#Iniciamos con la conexion al servidor, en una terminal de LINUX o desde un programa de terminal remoto, digitamos:

ssh root@IP_servidor_que envio_el_profesor

#Nos muestra un resultado similar al siguiente, donde mediante un yes verificamos que queremos realizar la conexion y nos preguntara sobre el password de #conexion (OJO) al escribir el password no podra ver los caracteres marcandose en pantalla.

ECDSA key fingerprint is SHA256:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'IP_servidor_que envio_el_profesor' (ECDSA) to the list of known hosts.
root@IP_servidor_que envio_el_profesor_password:
 
#Una vez dentro del servidor (note que la direccion de su shell cambio) procedemos al chequeo basico del servidor:

# Verificación del sistema operativo del servidor actual:

lsb_release -a

uname -a
 
# Comprobación y verificamos la conectividad básica del servidor:

dig
 
#El comando anterior nos muestra un resultado similar al siguiente:

; <<>> DiG 9.18.18-0ubuntu0.22.04.2-Ubuntu <<>>
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46985
;; flags: qr rd ra; QUERY: 1, ANSWER: 13, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;.				IN	NS

;; ANSWER SECTION:
.			39293	IN	NS	l.root-servers.net.
.			39293	IN	NS	e.root-servers.net.
.			39293	IN	NS	j.root-servers.net.
.			39293	IN	NS	k.root-servers.net.
.			39293	IN	NS	a.root-servers.net.
.			39293	IN	NS	d.root-servers.net.
.			39293	IN	NS	g.root-servers.net.
.			39293	IN	NS	m.root-servers.net.
.			39293	IN	NS	f.root-servers.net.
.			39293	IN	NS	b.root-servers.net.
.			39293	IN	NS	c.root-servers.net.
.			39293	IN	NS	h.root-servers.net.
.			39293	IN	NS	i.root-servers.net.

;; Query time: 3 msec
;; SERVER: 127.0.0.53#53(127.0.0.53) (UDP)
;; WHEN: Thu Mar 28 17:12:51 UTC 2024
;; MSG SIZE  rcvd: 239
 
# En caso de que no conozcamos la IP del servidor se puede obtener esta informacion basica mediante el siguiente comando:

ip a
 
#Otro aspecto importante de la verificacion basica del servidor es la identificación del espacio de almacenamiento libre:

df -lh
 
#Del comando anterior obtenemos una salida como la siguiene, donde es muy importante identificar si el servidor cuenta o no con espacio libre en las particiones necesarias (en este caso solo se ha usado el 7% del /):

Filesystem      Size  Used Avail Use% Mounted on
tmpfs            96M 1000K   95M   2% /run
/dev/vda1        25G  2.2G   22G   9% /
tmpfs           479M     0  479M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
/dev/vda15      105M  6.1M   99M   6% /boot/efi
tmpfs            96M  4.0K   96M   1% /run/user/0
 
#Una vez que se identificaron los aspectos basicos del servidor y conociendo que nada puede causar problemas y si los hay cuento con la informacion necesaria para resolverlos procedemos a la etapa de actualizar el servidor:

# Verificamos el respositorio de sistema de paqueteria

cat /etc/apt/sources.list

#El siguiente comando actualiza la lista de paquetes actualizables que cuenta el repositorio para brindarle al servidor:

apt update

#El siguiente comando muestra (en caso de que los alla) la lista de paquetes actualizables por el repositorio: 

apt list --upgradable

#El siguiente comando procede con el proceso de actualizacion de paquetes, instalacion y actualizacion de archivos (Y):

apt upgrade
 
#El comando anterior nos preguntara sobre si deseamos realizar la instalacion y nuestra respuesta es (Y)

#Luego detecta archivos que puede actualizar y podermos mantener o instalar la nueva version, no hay problemas en este momento. (OK)

#Por ser un tipo de VM en cloud pueda ser que nos de un mensaje de que se instalo un nuevo kernel que deberiamos reiniciar. (OK)

#Al finalizar la actualizacion se nos va a presentar un cuadro con una lista de servicios que utilizan librerias un poco antiguas (no se modifica la seleccion del cuadro) y simplemente le decimos (OK) para que reinicie los servicios pertinentes. 

#En la menu de dialogo respondemos "Keep the local version currently installed" (OK)

#Procedemos a crear usuario con privilegios, que no sea root, para tener una forma segura de conexion al servidor.
 
#El siguiente comando crea el usuario:

adduser nombre_de_su_preferencia

#El resultado del comando anterior nos da una salida como la siguiente donde aparte de escribir 2 veces el password para el nuevo usuario, no es necesario escribir informacion adicional (continuamos con la tecla ENTER).

Copying files from `/etc/skel' ...
Enter new UNIX password: 
Retype new UNIX password: 
passwd: password updated successfully
Changing the user information for stan
Enter the new value, or press ENTER for the default
	Full Name []: 
	Room Number []: 
	Work Phone []: 
	Home Phone []: 
	Other []: 
Is the information correct? [Y/n] 

#El siguiente comando obliga al usuario a cambiar su password en el siguiente login:

chage -d0 nombre_de_su_preferencia

#El siguiente comando agrega al nuevo usuario (con el nombre_de_su_preferencia) al grupo sudo para que pueda obtener privilegios administrativos:

usermod -a -G sudo nombre_de_su_preferencia

#Una vez que estamos seguros de la creacion del nuevo usuario y sus privilegios nos salimos del sistemas para ingresar con las nuevas credenciales.
 
Crtl ^D
 
#Note el cambio en la direccion de la terminal.

#Iniciamos nuevamente la conexion al servidor, en una termial de LINUX o desde un programa de terminal remoto, digitamos:

ssh nombre_de_su_preferencia@IP_servidor_que envio_el_profesor

#Nos pedira un cambio de password debido a que a la hora de crear el nuevo usuario utilizamos el comando chage, recuerde que los caracteres que escribe no seran mostrados.

#Basados en los comandos anteriores utilizamos el siguiente comando para ganar permisos de administrador del sistema:

sudo -s
 
#Luego de la ejecucion del comando anterior la terminal debe pasar del simbolo $ al simbolo # donde nos indica que somos administradores del sistema.

#Una vez que ya contamos con el nuevo usuario con privilegios eliminamos la opción de que root se pueda conectar por ssh
 
 #Editamos el archivo de la siguiente manera (Recuerde que para salir a modo administracion de archivo, salvar y salir se utiliza :wq!):

vi /etc/ssh/sshd_config

#Denttro del archivo y con la ayuda del buscador del editor nos posicionamos sobre la siguiente linea y la modificamos para que luzca igual a la esta:

PermitRootLogin no

#Una vez que estamos seguros de la modificacion de la linea guardamos y nos salimos de la edicion del archivo.

#Con el siguiente comando reiniciamos el demonio de ssh y lo obligamos a leer nuevamente su archivo de configuracion con los cambios que realizamos:

systemctl restart sshd

#Como siguiente paso procedemos a verificar el nombre actual del servidor con el siguiente comando:
 
hostnamectl

#Con el siguiente comando cambiamos el nombre del servidor:

hostnamectl set-hostname nombre_de_su_preferencia_para_el_servidor

#Volvemos a chequear el nombre del servidor y veremos que ahora debe ser igual al establecido en el paso anterior.

hostname

#OJO Hay que tener cuidado con el cambio de nombre del servidor ya que se vera reflejado en la ruta de la terminal hasta que se salga y se entre de nuevo al sistema.
 
#Instalar net-tools para poder usar el netstat mas adelante

apt install net-tools

#Con la ayuda del siguiente comando procedemos a verificar las conexiones activas a la maquina para descartar cualquier indicio de peligro:

netstat -tulpn
 
#Con la ayuda de cualquiera de los siguiente comandos procedemos a verificar los procesos actuales del sistema para descartar consumo excesivo de recursos, cabe recordar que lo importante a la hora de monitorear procesos es saber identificar los procesos (para esto utilizamos el PID y el comando que ejecuto ese proceso) saber el porcentaje de consumo de CPU y memoria:

ps -aux

#Con el comando anterior verificamos en consola y en modo texto los procesos del sistema.

htop

#Con el comando anterior verificamos en consola y de un modo mas amigable con el sysadmin los procesos del sistema de una manera mas detallada.

pstree

#Con el comando anterior verificamos en consola y de modo de arbol de procesos del sistema el consumo de recursos.
 
#El archivo fstab mantiene la organizacion de las particiones del sistema es importante en caso de ser un servidor en produccion conocer sus particiones actuales y su distribucion.

#Con el siguiente comando editamos el archivo en caso de requerirlo (en nuestro caso salga del archivo sin editarlo): 

vi /etc/fstab

#Instalación de anti-DOS en host, detector de rootkits y scans basicos al sistema con nmap.
 
 #El siguiente comando instala el paquete llamado fail2ban, respondemos Y:

apt install fail2ban

#Una vez finalizado el proceso de instalacion y configuracion procedemos a hacer una copia del archivo de configuracion del fail2ban.

cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

#El siguiente paso es editar el nuevo archivo de configuracion:

vi /etc/fail2ban/jail.local

#Dentro del archivo anterior buscamos las siguiente variables y las editamos para que se vean de la siguiente manera.

destemail = su_correo@aqui
action = %(action_mwl)s

#Luego de editar el archivo lo salvamos y salimos.

#Con el siguiente comando reiniciamos el servicio de fail2ban para que cargue la nueva configuracion:

/etc/init.d/fail2ban restart
  
#El siguiente comando instala los programas necesarios para la verificacion de presencia de rootkits en el sistema, respuestas (Y), 
En el cuadro de postfix configuration (OK)
(Internet Site) y (nombre_de_su_preferencia_para_el_servidor).

apt install rkhunter chkrootkit

#El siguiente comando determina si el sistema contiene indicios de precensia de rootkits. La salida deberia ser "nothing found" o "not infected" para los archivos analizados.

chkrootkit

#El siguiente comando determina si se han realizado variaciones y actualiza las propiedades de los archivos del sistema.

rkhunter --propupd

#El siguiente comando determina si el sistema contiene indicios de precensia de rootkits y ademas chequea los comandos del sistema a ver si han sido modificados (5 veces ENTER para continuar). La salida deberia ser "not found" o "OK"

rkhunter --check

#Ejemplo de salida del comando anterior

System checks summary
=====================

File properties checks...
    Files checked: 142
    Suspect files: 0

Rootkit checks...
    Rootkits checked : 498
    Possible rootkits: 0

Applications checks...
    All checks skipped
 
#Chequeo de seguridad 1, uso basico del nmap (Herramienta de seguridad para explorar la red y escanear puertos)
 
#El siguiente comando instala nmap, respuesta (Y).

apt install nmap

#Este comando realiza un scan de paquetes TCP y verbose mode sobre localhost (puerto 22 y 25 abiertos):

nmap -v -sT localhost

#Ejemplo de salida del comando anteriror

Nmap scan report for localhost (127.0.0.1)
Host is up (0.0023s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 998 closed ports
PORT   STATE SERVICE
22/tcp open  ssh
25/tcp open  smtp

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 0.12 seconds
           Raw packets sent: 0 (0B) | Rcvd: 0 (0B)


#Este comando realiza un scan de paquetes SYNP y verbose mode sobre localhost (puerto 22 y 25 abiertos):

nmap -v -sS localhost

------------------------------------------------------------------------------------------------------------------------------------
 
#Requerimientos del CMS

#PHP

#Este comando instala y verifica los paquetes de php y sus requerimientos, necesarios para que los otros servicios y programas se ejecuten de la mejor manera, respuesta (Y):
 
apt install php-cli php-fpm php-mysql php-json php-opcache php-mbstring php-xml php-gd php-curl libphp-embed php-dompdf php-gd php-xml php-mysql libapache2-mod-php php-mysql php-xml

#revision de la version de php.

php --version

#Listar todos los modulos cargados de php

php -m
 
#Una vez finalizada la instalacion con el siguiente comando verificamos que el php se encuentra "corriendo" y deberiamos ver el servicio en color verde, (Active: active (running)):

systemctl status php8.1-fpm

#Con el siguiente comando editamos las variables de la php necesarias para que el CMS trabaje adecuadamente:

vi /etc/php/8.1/cli/php.ini

#Dentro del archivo anterior buscamos las siguiente variables y las editamos para que se vean de la siguiente manera.

file_uploads = On
allow_url_fopen = On
memory_limit = 256M
upload_max_filesize = 100M
max_execution_time = 360

#Luego de editar el archivo lo salvamos y salimos.

#hacer los cambios tambien para el archivo es la siguiente ubicacion.

vim /etc/php/8.1/apache2/php.ini

#Luego de editar el archivo lo salvamos y salimos.

#revisamos la syntaxis de los cambios.

systemctl reload apache2

Reiniciamos ahora si que estamos seguros que no se va a danar nada.

systemctl restart apache2

#Apache2

#El siguiente comando instala el servicio encargado del servidor web, para esta etapa ya a sido instalado y configurado como un requerimiento de los paquetes de php y solamente verificamos que este realmente instalado.
 
apt install apache2

#Con el siguiente comando reiniciamos el servicio de apache2 para que cargue la nueva configuracion de php que hicimos anteriormente:

systemctl restart apache2.service

#En un navegador web con acceso a Internet colocamos en la URL la direccion IP de nuestro servidor.

http://your_server_IP_address (ip a, para ver su IP)

#Deberiamos observar la pagina de configuracion por defecto del Apache2

#Con el siguiente comando habilitamos en modulo (rewrite) que va a ser necesario para el CMS.

a2enmod rewrite

#Con el siguiente comando reiniciamos el servicio de apache2 para que cargue la nueva configuracion que hicimos anteriormente:

systemctl restart apache2
 
#Editamos con el siguiente comando el archivo de seguridad.

vi /etc/apache2/conf-available/security.conf

#Dentro del archivo anterior buscamos las siguiente variables y las editamos para que se vean de la siguiente manera.

ServerTokens Prod
ServerSignature Off
TraceEnable Off

#Luego de editar el archivo lo salvamos y salimos.

#Con el siguiente comando reiniciamos el servicio de apache2 para que cargue la nueva configuracion que hicimos anteriormente.

systemctl restart apache2
 
#Chequeo de seguridad 2, nmap (Herramienta de seguridad para explorar la red y escanear puertos), Puertos 22, 25 y 80 abiertos.

nmap -sS -PN -sV -A -O IP_del_Servidor
 
#Mysql

#Instalacion del DBMS MYSQL.

#El siguiente comando instala el servidor el cliente y el common para poder utilizar el servicio de base de datos para el CMS, respuesta (Y).
 
apt install mysql-server mysql-client mysql-common
 
#El siguiente comando detiene el servicio de mysql.

systemctl stop mysql.service

#El siguiente comando inicia el servicio de mysql.

systemctl start mysql.service

#El siguiente comando habilita el mysql para iniciar automaticamente al iniciar el servidor.

systemctl enable mysql.service
 
#Una vez que el servicio esta en ejecucion se corre con el siguiente comando el script de seguridad para MYSQL:
 
mysql_secure_installation
 
#El script solicitara una serie de respuestas, las siguientes son las respuestas que brindaremos.

Press y|Y for Yes, any other key for No: NO
 
y
y
y
y
 
#El siguiente comando me da acceso a el DBMS, utilice la clave de root del sistema.
 
mysql -u root -p

#Una vez que ingresamos al mysql note el cambio de la ruta de la terminal (mysql>).

#Dentro de mysql creamos la base de datos con el siguiente comando:

CREATE DATABASE nombre_de_base_de_datos;

#El resultado debe ser un OK

#Luego con el siguiente comando creamos el usuario y lo asociamos al password (mantenga las comillas):

CREATE USER 'nombre_usuario_de_base_de_datos'@'localhost' IDENTIFIED BY 'password_usuario_para_la_base_de_datos';

#El resultado debe ser un OK

#Luego asociamos la base de datos con el usuario y el password:

GRANT ALL ON nombre_de_base_de_datos.* TO 'nombre_usuario_de_base_de_datos'@'localhost';

#El resultado debe ser un OK

#Luego recargamos los permisos.

FLUSH PRIVILEGES;

#El resultado debe ser un OK

#Salimos de mysql con el siguiente comando y deberiamos notar en cambio en la ruta de la terminal (#)

EXIT;
 
#Note que volvimos a la terminal bash (#)

#Con el siguiente comando verificamos que el nombre_usuario_de_base_de_datos y nombre_de_base_de_datos existan y tengan acceso.
 
mysql -u nombre_usuario_de_base_de_datos -p
 
#El password que nos solicita el comando anterior es password_usuario_para_la_base_de_datos.

#Una vez mas dentro de la consola de mysql (bede cambiar nuevamente la ruta de la terminal ) verificamos la conexion de la base de datos para CMS con el siguiente comando:
 
mysql> show databases;
 
#Deberiamos ver la base de datos con el nombre que seleccionamos arriba como valor de nombre_de_base_de_datos.

#Salimos de mysql con el siguiente comando y deberiamos notar en cambio en la ruta de la terminal (#)

EXIT;

#CMS

#Drupal

#Con el siguiente comando nos movemos a la carpeta /tmp y descargamos con el comando wget la version de drupal que vamos a utilizar.
 
cd /tmp && wget https://ftp.drupal.org/files/projects/drupal-10.2.4.tar.gz
 
#El siguiente comando se utiliza para descomprimir el archivo .tar.gz del CMS

tar -zxvf drupal*.gz
 
#Esto nos creara una carpeta llamada drupal-10.2.4 con todo el contenido del archivo que anteriormente estaba agrupado y comprimido. Lo movemos a una carpeta mas representativa que llamaremos drupal-10 solamente.

mv drupal-10.2.4 drupal-10

#En el siguiente comando se mueve la carpeta drupal-10 a la ruta donde el servicio de Apache2 pondra a disposicion de Internet el nuevo contenido que se llama DocumentRoot del VirtualHost y se renombra con el movimiento a drupal-9.

mv drupal-10 /var/www/html/drupal-10
 
#En este siguiente comando nos cambiamos de directorio donde esta la ubicación de la carpeta del CMS en la base del apache2, y listamos su contenido (con el fin de revisar permisos, podemos notar que todo le pertenece a root).

cd /var/www/html/drupal-10
 
#Con el siguiente comando verificamos los permisos (owner y group) de la carpeta del CMS, como todo perteneca a root debemos cambiarlo para que apache2 sea capaz de manejarlo

ls -la
 
#Con el siguiente comando se realiza el cambio de owner de manera recursiva para que el owner:group sea el apache2 sobre la carpeta entera que contiene el sistema CMS.
 
chown -R www-data:www-data /var/www/html/drupal-10

#Con el siguiente comando verificamos los permisos (owner y group) de la carpeta del CMS y verificamos que ahora todo pertenece al usuario y grupo www:data quien es encargado de manejar el apache2

ls -la

#Con el siguiente comando cambiamos los permisos para que el owner tenga rwx, group tenga r_x y Others tenga r_x de esta manera el apache2 podra hacer los cambios necesarios y tener control total del sistema para Internet.

chmod -R 755 /var/www/html/drupal-10/

#El siguiente comando realiza un copia de seguridad (conservando los metadatos) para realizar sobre esta las modificaciones del caso al CMS por parte del apache2.
 
cp -a /var/www/html/drupal-10/sites/default/default.settings.php /var/www/html/drupal-10/sites/default/settings.php
 
#Con el siguiente comando reiniciamos el servicio de apache2 para que cargue la nueva configuracion que hicimos anteriormente.

systemctl restart apache2.service
 
#Configuración de Virtual host

#Modificaciones a los archivos del apache2 para publicar el CMS a el Internet.
 
#Con el siguiente comando nos cambiamos de directorio a la carpeta donde Apache2 mantiene la configuracion de los sitios web que tiene publicos a la Internet.

cd /etc/apache2/sites-available

#Con el siguiete comando realizamos una copia de la configuracion estandar de apache2 para un sitio web y la nombramos con el mismo nombre de la carpeta que creamos con el contenido del CMS (para nuestro ejercicio es drupal-10).

cp 000-default.conf drupal-10.conf

#Con el siguiente comando nos vamos a la carpeta donde esta la configuracion de el nuevo sitio web.

cd ../sites-enabled

#El siguiente comando hace un link simbolico a la carpeta de los sitios publicados en Intertet con la nueva configuracion de mi sitio.

ln -s ../sites-available/drupal-10.conf

#Solo para estar seguros el siguiente comando nos deberia decir que ya la configuracion del nuevo sitio esta habilitada.

a2ensite drupal-10.conf

#El siguiente comando desabilita la configuracion del sitio por defecto del apache2

a2dissite 000-default.conf

#El siguiente comando elimina la pagina por defecto que nos mostro el apache2 la primera vez que accesamos el servidor desde el navegador web.

rm /var/www/html/index.html

#Con el siguiente comando reiniciamos el servicio de apache2 para que cargue la nueva configuracion que hicimos anteriormente.

systemctl restart apache2.service

#Con el siguiete comando editamos la configuracion del nuevo sitio web del CMS para que nos permita manejar el drupal desde la interface web y ya no desde la terminal como lo hemos estado haciendo hasta este momento.

vi /etc/apache2/sites-enabled/drupal-10.conf
 
#Dentro del archivo anterior buscamos las siguiente variables y las editamos para que se vean de la siguiente manera, si no existen las agregamos en el mismos lugar donde se encuentran abajo, las lineas que no aparecen en el texto abajo se dejan igual.
 
        DocumentRoot /var/www/html/drupal-10
 
        <Directory /var/www/html/drupal-10/>
                   Options +FollowSymlinks -Indexes
                   AllowOverride All
                   Require all granted
        </Directory>
 
        <Directory /var/www/html/>
                    RewriteEngine on
                    RewriteBase /
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteCond %{REQUEST_FILENAME} !-d
                    RewriteRule ^(.*)$ index.php?q=$1 [L,QSA]
        </Directory>
 
#Luego de editar el archivo lo salvamos y salimos.

#Con el siguiente comando reiniciamos el servicio de apache2 para que cargue la nueva configuracion que hicimos anteriormente.

systemctl restart apache2.service
 
#CMS Drupal desde la interface web

#Procedemos a ir al navegador web que en anteriores pasos utilizamos y recargamos la direccion IP del servidor, seguir los pasos y configurar lo que le va indicando el drupal. 

#Configuracion de Drupal, Choose language = English, Profile = Standard, Database = nombre_de_base_de_datos, nombre_usuario_de_base_de_datos y password_usuario_para_la_base_de_datos, Site name = el_nombre_que_quiera, Site email address = email_valido, Username = usuario_admin_web_del_CMS, Password = password_del_usuario_admin_web_del_CMS.

#Listo ya esta el CMS instalado.

#ahora volvemos a la terminal del servidor.

# Script_de_Permisos

#Una vez que chequeamos que todo esta trabajando bien modificamos los permisos para poder usar el drupal sin tantos riesgos de seguridad.

#Con el siguiente comando nos movemos al home de nuestro usuario.

cd 

#En los archivos del gitlab donde se encuantran estas intrucciones estan otros 2 archivos, busque el que se llama Script_de_Permisos y copie su contenido.

#Con el siguiente comando editamos el archivo Script_de_Permisos y pegamos el contenido.

vi Script_de_Permisos

#Estando seguro de los cambios salvamos y salimos del archivo.

#Con el siguiente comando le damos permisos de ejecucion a el script de permisos

chmod +x Script_de_Permisos

#Luego con el siguiete comando ejecutamos el script para modificar los permisos

./Script_de_Permisos --drupal_path=/var/www/html/drupal-10/ --drupal_user=root

#Luego nos vamos a la carpeta del drupal.

cd /var/www/html/drupal-10/

#Luego con el siguiete comando verificamos los cambios en los permisos ahora no deberian ser todos los archivos de www:data:www:data.

ls -la

#Con el siguiene comando nos movemos al home del usuario actual para proseguir con la configuracion del firewall

cd 

#Firewall

#IP_Tables

#Con el siguiente comando nos movemos al home de nuestro usuario.

cd 

#En los archivos del gitlab donde se encuantran estas intrucciones estan otros 2 archivos, busque el que se llama Script_de_Firewall y copie su contenido.

#Con el siguiente comando editamos el archivo Script_de_Firewall y pegamos el contenido.

vi Script_de_Firewall

#cambiamos las IP por las IPs que necesitamos.

#Estando seguro de los cambios salvamos y salimos.

#Con el siguiente comando le damos permisos de ejecucion a el script de Firewall

chmod +x Script_de_Firewall

#El siguiente comando instala los paquetes necesarios para poder configurar un firewall en el localhost, respondemos yes en ambas solicitudes:      

apt install iptables-persistent netfilter-persistent

#En el cuadro de dialogo de Configuring iptables-persistent respondemos (Yes) para ambos IPv4 e IPv6.

#Con el siguiente comando verificamos que el iptables y sus reglas esten siendo aplicadas por el servicio.

netfilter-persistent start

#Con el siguiene comando detenemos el servicio.

systemctl stop netfilter-persistent

#Con el siguiene comando iniciamos el servicio.

systemctl start netfilter-persistent

#Con el siguiene comando reiniciamos el servicio.

systemctl restart netfilter-persistent

#Con el siguiente comando revisamos las reglas actuales que tiene cargadas el iptables.

iptables -L 

#Con el siguiente comando ejecutamos el script para que modifique las reglas del firewall.

./Script_de_Firewall

#Con el siguiente comando revisamos las reglas actuales que tiene cargadas el iptables y las comparamos con las que anteriormente contaba.

iptables -L 

#Salvamos nuestras nuevas reglas con:

netfilter-persistent save

#Este comando realiza un scan de paquetes y puertos abiertos y deberiamos ver una diferencia sustancias comparado con las veces anteriores.

#Ejecutamos el comando nmap despues de cargar las nuevas reglas del Firewall

nmap -sS -PN -O -A -sV  localhost_o_IP_del_servidor

#Deberiamos ver los puertos abiertos y todos los demas filtrados por el firewall

#Muchas gracias por su trabajo y esfuerzo.
